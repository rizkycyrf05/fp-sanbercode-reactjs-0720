import React from 'react';
import Main from "./Layouts/Main";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { UserProvider } from "./Contexts/UserContext";

function App() {
  return (
    <UserProvider>
      <Main />
    </UserProvider>
  );
}

export default App;
