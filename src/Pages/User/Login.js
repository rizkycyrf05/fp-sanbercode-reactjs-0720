import React, { useContext, useState, useEffect } from "react"
import { UserContext } from "../../Contexts/UserContext";
import { Link } from "react-router-dom";
import { Form, Button, Card } from "react-bootstrap";
import axios from "axios";

const Login = () => {
    const [, setUser] = useContext(UserContext);
    const [input, setInput] = useState({ username: "", password: "" });
    const [listUser, setListUser] = useState(null);

    const handleSubmit = (e) => {
        e.preventDefault();
        const cekUser = listUser.find(el => el.username === input.username);
        if (input.username.replace(/\s/g, "") !== "" && input.password.replace(/\s/g, "") !== "") {
            if (cekUser === undefined) {
                alert("Sing In Successfully!");
            } else {
                if (cekUser.password === input.password) {
                    setUser({ username: input.username, password: input.password, id: cekUser.id });
                    // console.log(user);
                    localStorage.setItem("user", JSON.stringify(cekUser));
                } else {
                    alert("Username Or Password Is Wrong");
                }
                console.log(cekUser);
            }
        } else {
            alert('You Must Fill In The Form!');
        }
    }

    const handleChange = (e) => {
        setInput({ ...input, [e.target.name]: e.target.value });
    }

    useEffect(() => {
        if (listUser === null) {
            axios.get(`https://backendexample.sanbersy.com/api/users`)
                .then((res) => {
                    setListUser(res.data.map(el => {
                        return {
                            username: el.username,
                            password: el.password,
                            id: el.id
                        }
                    }));
                });
        }
    });

    return ( 
        <Card
            border="primary"
            style={{ width: '20rem' }}
        >
        <Card.Header><h1 align="center">Login</h1></Card.Header>
        <Card.Body>
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" value={input.username} placeholder="Username..." name="username" onChange={handleChange} />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" value={input.password} placeholder="Password" name="password" onChange={handleChange} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Sign In
                </Button>
            </Form>
        </Card.Body>
        </Card>
    )
}

export default Login
