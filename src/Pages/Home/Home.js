import React from "react"
import Movies from "../Home/Movies";
import Game from "../Home/Game";

const Home = () => {
    return (
        <>
            <Movies />
            <Game />
        </>
    )
}

export default Home;
