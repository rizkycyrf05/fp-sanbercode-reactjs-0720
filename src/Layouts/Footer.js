import React from "react";
import { Container } from "react-bootstrap";

class Footer extends React.Component {
    render() {
        return (
            <footer>
                <h5>Copyright &copy; 2020 by DikitaReview</h5>
            </footer>
        );
    }
}

export default Footer;