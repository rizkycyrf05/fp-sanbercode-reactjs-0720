import React from "react";
import { ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";

class Sidebar extends React.Component {
    render() {
        return (
            <ListGroup>
                <ListGroup.Item variant="dark">
                    <h1>Profile</h1>
                    <ol>
                        <li>
                            <strong>Nama : </strong>Nur Ridho Rizki
                        </li>
                        <li>
                            <strong>Email : </strong>rizkycyrf05@gmail.com
                        </li>
                        <li>
                            <strong>Sistem Operasi yang digunakan : </strong>Windows10
                        </li>
                        <li>
                            <strong>Akun Gitlab : </strong>rizkycyrf05
                        </li>
                        <li>
                            <strong>Akun Telegram : </strong>https://t.me/Rizky_Cyrf
                        </li>
                    </ol>
                </ListGroup.Item>
                <h1>Add</h1>
                {this.props.menu.map(el => {
                    return (
                        <ListGroup.Item>
                            <Link to={'/' + this.props.parent + '/' + el.toLowerCase().replace(' ', '-')}>{el}</Link>
                        </ListGroup.Item>
                    );
                })}
            </ListGroup>
        );
    }
}

export default Sidebar;