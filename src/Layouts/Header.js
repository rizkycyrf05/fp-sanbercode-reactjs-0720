import React from "react";
import { Form, Navbar, Button, Container, Nav, } from "react-bootstrap";
import { Link } from "react-router-dom";
import { UserContext } from "../Contexts/UserContext";
import { useContext } from "react";

const Header = () => {
    const [user, setUser] = useContext(UserContext);

    const handleLogout = () => {
        setUser(null)
        localStorage.removeItem("user")
    }
    return (
        <Navbar>
            <Container>
                <Navbar.Brand>
                    <Link to="/">Home</Link>
                </Navbar.Brand>
                <Navbar.Collapse id="basic-navbar-nav">
                    {user !== null &&
                        <Nav className="mr-auto">
                            <Nav.Link>
                                <Link to="/movies">Movies</Link>
                            </Nav.Link>
                            <Nav.Link href="#link">
                                <Link to="/games">Games</Link>
                            </Nav.Link>
                        </Nav>
                    }
                </Navbar.Collapse>
                <Form inline>
                    {user === null &&
                        <>
                            <Button variant="primary">
                                <Link to="/login">Login</Link>
                            </Button>    
                            <Button variant="primary">
                                <Link to="/register">Register</Link>
                            </Button>
                        </>}
                    {user !== null &&
                        <>
                            <Button variant="dark">
                                <Link to="/change-password">Change Password</Link>
                            </Button>
                            <Button variant="light" onClick={handleLogout}>
                                Logout
                            </Button>
                        </>}
                </Form>
            </Container>
        </Navbar>
    );
}

export default Header;